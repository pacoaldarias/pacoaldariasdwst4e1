<?php
include_once('Config.php');
include_once("funciones.php");
include_once("Ficheros.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>


        <?php cabecera(); ?>

        <h3>Gestión de Enlaces. </h3>
        <h4>Autor Profesor: Paco Aldarias</h4>

        <p><a href="FormularioE.php">Alta</a> </p>

        <?php
        echo '<table border="1">';

        echo '<tr>';
        echo '<th>';
        echo 'Id';
        echo '</th>';

        echo '<th>';
        echo 'Nombre';
        echo '</th>';

        echo '<th>';
        echo 'Url';
        echo '</th>';

        echo '</tr>';


        $enlaces = array();
        $enlaces = getEnlaces();

        if (count($enlaces) > 0) {

            foreach ($enlaces as $enlace) {
                echo "<tr>";
                echo "<td>" . $enlace["id"] . "</td>";
                echo "<td>" . $$enlace["nombre"] . "</td>";
                echo "<td>" . $$enlace["url"] . "</td>";
                echo "</tr>";
            }
        }

        echo "</table >";
        ?>
        <?php inicio(); ?>
        <?php pie(); ?>

    </body>
</html>
