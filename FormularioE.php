<?php
include_once('Config.php');
include_once("funciones.php");
include_once("Ficheros.php");
?>

<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        ?>

        <?php cabecera(); ?>
        <h3>Alta Enlace</h3>

        <form action="MenuE.php" method="post">
            <table>
                <tr>
                    <td>Id</td>
                    <td><input type="text" name="id" value="1"/> </td>
                </tr>
                <tr>
                    <td>Nombre</td>
                    <td><input type="text" name="nombre" value="CEED"/></td>
                </tr>
                <tr>
                    <td>Url</td>
                    <td><input type="text" name="url" value="http://www.ceedcv.es"/></td>
                </tr>

                <tr>
                    <td>Tipo Enlace</td>
                    <td><input type="text" name="tipoenlace" value="1"/></td>
                </tr>




            </table>

            <table>
                <tr>
                    <td>
                        <input type="submit" value="Enviar" />
                    </td>
                    <td>
                        <input type="reset" value="Borrar" />
                    </td>
                </tr>
            </table>
        </form>

        <?php
        $id = recoge("id");
        if ($id == "")
            $id = "Error: Vacio";

        $nombre = recoge("nombre");
        if ($nombre == "")
            $nombre = "Error: Vacio";

        $url = recoge("url");
        if ($url == "")
            $url = "Error: Vacio";

        $tipoenlace = recoge("tipoenlace");
        if ($tipoenlace == "")
            $tipoenlace = "Error: Vacio";

        $enlace = array();
        $enlace["id"] = $id;
        $enlace["nombre"] = $nombre;
        $enlace["url"] = $url;
        $enlace["tipoenlace"] = $tipoenlace;

        grabarEnlace($enlace);
        ?>


        <?php inicio(); ?>
        <?php pie(); ?>

    </body>
</html>